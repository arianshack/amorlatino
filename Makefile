all: wellcome
	@if [ ! -f "src/Makefile" ]; then \
		echo "Makefile not found, run configure script before compile the code."; \
		exit; \
		else \
		cd src; $(MAKE); $(MAKE) lang; \
		cd ..; \
	fi


wellcome:

	@echo "****************************************"
	@echo "*** { Innovating, Making The World } ***"
	@echo "****************************************"
clean:
	@if [ ! -f "src/Makefile" ]; then \
		echo "Makefile not found, run configure script before compile the code."; \
		exit; \
		else \
		cd src; $(MAKE) clean; \
		cd ..; \
	fi
